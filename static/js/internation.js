function langInit() {



    $(function () {



        i18next
            .use(window.i18nextHttpBackend)
            .use(window.i18nextBrowserLanguageDetector)
            .init({
                debug: true,
                // lng: 'fr',
                backend: {
                    loadPath: '/locales/{{lng}}.json'
                },
                supportedLngs: ['en', 'fr'],
                fallbackLng: 'en',
                // debug: true,
                lng: 'fr', // evtl. use language-detector https://github.com/i18next/i18next-browser-languageDetector
                detection: {
                    order: ['querystring', 'cookie', 'localStorage', 'sessionStorage', 'navigator', 'htmlTag', 'path', 'subdomain'],
                    htmlTag: document.documentElement,
                }, 
            }, function (err, t) {

                if (err) return console.error(err);

                // for options see
                // https://github.com/i18next/jquery-i18next#initialize-the-plugin
                jqueryI18next.init(i18next, $, { useOptionsAttr: true });

                // start localizing, details:
                // https://github.com/i18next/jquery-i18next#usage-of-selector-function

                $('head').localize();
                $('body').localize();


            });
    }); 
}