var tts = {};
tts.Synth = window.speechSynthesis;

tts.DvIndex = 0;
tts.DvRate = 0.7;
tts.Cancel = true;
tts.readFrags = false;
tts.readNotes = false;


const voicebyLang = lang => speechSynthesis
    .getVoices()
    .find(voice => voice.lang.startsWith(lang))


const pickVoice = () => { 
    return voicebyLang(i18next.resolvedLanguage);
}

const delay = ms => new Promise(res => setTimeout(res, ms));

// FIX A GLITCH 
var myTimeout;
function myTimer() {
    window.speechSynthesis.pause();
    window.speechSynthesis.resume();
    myTimeout = setTimeout(myTimer, 10000);
}

tts.ReadText = function (txt) {
    let utterance = new SpeechSynthesisUtterance(txt);

    utterance.onstart = function (event) {
        myTimer();
    };

    utterance.onend = async function (event) {
        clearTimeout(myTimeout);
        await delay(2000);
        if(ttsOn()){
            Reveal.next(); 
            // const event = new Event("slidechanged");
            // Reveal.dispatchEvent(event);
        }
    };

    utterance.voice = pickVoice();

    utterance.rate = tts.DvRate;
    tts.Synth.speak(utterance);
};



const ttsOn = () => $('.speaker').hasClass('on')

tts.ToggleSpeech = function () {
    $('.speaker').toggleClass('on');
    if (ttsOn()) {
        // tts.ReadSimple("speech On!");
        var thisSlide = Reveal.getCurrentSlide();
        tts.ReadAnyElmts(thisSlide, ".notes"); 
 
    } else { 
        tts.Synth.cancel();
        tts.ReadText("speech Off!")
    };
}; 

tts.ReadSimple = function (txt) {
    let utterance = new SpeechSynthesisUtterance(txt);
 
    utterance.voice = pickVoice();

    utterance.rate = tts.DvRate;
    tts.Synth.speak(utterance);
};


tts.ReadVisElmts = function () {
    let focusElmt = arguments[0];
    for (let i = 1; i < arguments.length; i++) {
        let xElmts = focusElmt.querySelectorAll(arguments[i]);
        for (let k = 0; k < xElmts.length; k++) {
            tts.ReadText(xElmts[k].innerText);
        }
    }
};

tts.ReadAnyElmts = function () {
    let focusElmt = arguments[0];
    for (let i = 1; i < arguments.length; i++) {
        let xElmts = focusElmt.querySelectorAll(arguments[i]);
        for (let k = 0; k < xElmts.length; k++) {
            tts.ReadText(xElmts[k].textContent);
        }
    }
};



